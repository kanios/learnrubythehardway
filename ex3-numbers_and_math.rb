plus = 5 + 5
minus = 5 - 5
slash = 5 / 5
asterisk = 5 * 5
percent = 5 % 5
less = 5 < 5
greater = 5 > 5
lesseq = 5 <= 5
greatereq = 5 >= 5

puts "5 + 5 = #{plus}"
puts "5 - 5 = #{minus}"
puts "5 / 5 = #{slash}"
puts "5 * 5 = #{asterisk}"
puts "5 % 5 = #{percent}"
puts "5 < 5 = #{less}"
puts "5 > 5 = #{greater}"
puts "5 <= 5 = #{lesseq}"
puts "5 >= 5 = #{greatereq}"
